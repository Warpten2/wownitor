#ifndef Root_h__
#define Root_h__

#include "BLTEFile.hpp"
#include "DataTypes.hpp"
#include "Util.hpp"
#include "NetworkStream.h"
#include "Crypto.hpp"

class CDNHandler;

#include <string>
#include <unordered_map>
#include <array>

class Root : public BLTEFile
{
private:
    enum LocaleFlags : unsigned int
    {
        All = 0xFFFFFFFF,
        NoLocale = 0,
        //Unk_1 = 0x1,
        enUS = 0x2,
        koKR = 0x4,
        //Unk_8 = 0x8,
        frFR = 0x10,
        deDE = 0x20,
        zhCN = 0x40,
        esES = 0x80,
        zhTW = 0x100,
        enGB = 0x200,
        enCN = 0x400,
        enTW = 0x800,
        esMX = 0x1000,
        ruRU = 0x2000,
        ptBR = 0x4000,
        itIT = 0x8000,
        ptPT = 0x10000,
        enSG = 0x20000000, // custom
        plPL = 0x40000000, // custom
        All_WoW = enUS | koKR | frFR | deDE | zhCN | esES | zhTW | enGB | esMX | ruRU | ptBR | itIT | ptPT
    };

    enum ContentFlags : unsigned int
    {
        NoContentFlags= 0,
        LowViolence = 0x80, // many models have this flag
        NoCompression = 0x80000000 // sounds have this flag
    };

public:
    explicit Root(std::string const& fileUrl, CDNHandler* buildInfo) : BLTEFile(fileUrl, buildInfo)
    {
        Load(local_file());
    }

    void Load(Local::Buffer* decompressedFile) override;
    
    struct Block
    {
        ContentFlags Content;
        LocaleFlags Locale;
    };

    struct Entry
    {
        Block RootBlock;
        int FileDataID;
        std::array<unsigned char, 16> MD5;
    };

    bool HasEntry(uint64_t hashString);
    Entry const& GetEntry(uint64_t hashString);
    Entry const& GetEntry(std::string const& hashString) { return GetEntry(Crypto::ComputeHash(hashString)); }
private:
    typedef std::array<unsigned char, 16> Hash;

    std::unordered_map<uint64_t, Entry> _entries;
};

#endif // Root_h__
