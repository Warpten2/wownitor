#include "Encoding.h"
#include "DataTypes.hpp"
#include "Util.hpp"
#include <sstream>

void Encoding::Load(Local::Buffer* stream)
{
    short Signature;
    *stream >> Signature;
    
    stream->skip(7);

    int numEntries = 0;
    *stream >> BigEndian<4>(numEntries);
    
    stream->skip(5);
    
    int stringBlockSize = 0;
    *stream >> BigEndian<4>(stringBlockSize);

    stream->skip(stringBlockSize + 0x20 * numEntries); // Skip string block and table header

    int currentPosition = stream->position();

    for (int i = 0; i < numEntries; ++i)
    {
        unsigned short keysCount;
        *stream >> keysCount;

        while (keysCount != 0)
        {
            std::array<unsigned char, 16> entryKey;
            Entry entry;
            *stream >> BigEndian<4>(entry.FileSize) >> entryKey >> entry.Key;
            if (keysCount > 1)
                stream->skip(16 * (keysCount - 1));
            _entries[entryKey] = entry;

            *stream >> keysCount;
        }

        // Skip padding until next field
        int paddingLength = 4096 - (stream->position() - currentPosition) % 4096;
        if (paddingLength > 0)
            stream->skip(paddingLength);
    }
}

bool Encoding::HasEntry(std::array<unsigned char, 16> const& hashString) const
{
    return _entries.find(hashString) != _entries.end();
}

Encoding::Entry const& Encoding::GetEntry(std::array<unsigned char, 16> const& hashString) const
{
    return _entries.at(hashString);
}
