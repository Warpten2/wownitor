#include "NetworkStream.h"

#include <boost/asio.hpp>

Network::Stream::Stream(const std::string& fileUrl)
{
    set_option(Network::Option::Accept());
    open(fileUrl);
}

Network::Stream::Stream()
{
    set_option(Network::Option::Accept());
}

bool Network::Stream::open(const std::string& fileUrl)
{
    std::string url = fileUrl;
    auto protocolItr = std::find(url.begin(), url.end(), ':');

    std::string protocol;
    protocol.assign(url.begin(), protocolItr);

    if (*protocolItr++ != ':')
        return false;
    if (*protocolItr++ != '/')
        return false;
    if (*protocolItr++ != '/')
        return false;

    auto hostItr = std::find(protocolItr, url.end(), '/');
    
    std::string host;
    host.assign(protocolItr, hostItr);
    std::string query;
    query.assign(++hostItr, url.end());
    
    _stream.close();
    _stream.expires_at(boost::posix_time::pos_infin);
    _stream.connect(host, protocol);
    if (!_stream)
        return false;

    _stream << "GET /" << query << " HTTP/1.0\r\n";
    _stream << "Host: " << host << "\r\n";
    for (std::vector<Network::NetworkOption>::const_iterator itr = _options.begin(); itr != _options.end(); ++itr)
        _stream << itr->name() << ": " << itr->body() << "\r\n";
    _stream << "Connection: close\r\n\r\n" << std::flush;

    std::string http_version;
    _stream >> http_version;
    unsigned int status_code;
    _stream >> status_code;
    std::string status_message;
    std::getline(_stream, status_message);
    if (!_stream || http_version.substr(0, 5) != "HTTP/" || (status_code != 200 && status_code != 206))
        return false;

    // Process the response headers.
    std::string header;
    while (std::getline(_stream, header) && header != "\r")
    {
        auto delim = header.find(':');
        if (header.substr(0, delim) == "Content-Length")
            _content_length = std::stoull(header.substr(delim + 1));
    }

    return true;
}

void Network::Stream::set_option(Network::NetworkOption const& option)
{
    _options.push_back(option);
}