#ifndef DataTypes_h__
#define DataTypes_h__

#include <istream>
#include <array>

// Big Endian:
// 01 02 03 04 -> 01020304
template <size_t N> struct BigEndian
{
    friend class BinaryStream;

private:
    unsigned char* Data;

public:
    BigEndian()
    {
        Data = (unsigned char*)::malloc(N);
    }

    template<typename T>
    BigEndian(T& integer)
    {
        Data = reinterpret_cast<unsigned char*>(&integer);
    }

    // Assumes platform is LE.
    unsigned char& operator [] (int i)
    {
        return Data[(N - 1) - i];
    }
};

#endif // DataTypes_h__