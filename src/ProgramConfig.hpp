#ifndef ProgramConfig_h__
#define ProgramConfig_h__

#include "Tokenizer.hpp"
#include "Monitor.hpp"

#include <boost/filesystem.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/detail/xml_parser_writer_settings.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>

#include <unordered_map>
#include <string>
#include <locale>

class ProgramConfig
{
public:
    static ProgramConfig* instance()
    {
        static ProgramConfig instance;
        return &instance;
    }

    bool Loaded() { return _loaded; } const

    void Set(std::string const& name, int v)
    {
        _intOptions[name] = v;
    }

    void Set(std::string const& name, std::string const& v)
    {
        _strOptions[name] = v;
    }

    std::string const& GetString(std::string const& name) const
    {
        return _strOptions.at(name);
    }

    int GetInt(std::string const& name) const
    {
        return _intOptions.at(name);
    }

    template <typename T>
    using ConfContainer = std::unordered_map<std::string, T>;

    ConfContainer<int> _intOptions;
    ConfContainer<std::string> _strOptions;
    int _version;

private:

    ProgramConfig()
    {
        _intOptions.insert(std::make_pair("region", 1));
        _strOptions.insert(std::make_pair("channel", "wow_beta"));

        _loaded = read();
        if (!_loaded)
            write();
    }

    bool read()
    {
        boost::filesystem::path p = boost::filesystem::initial_path();
        p /= "Config.xml";

        if (!boost::filesystem::exists(p))
            return false;

        using boost::property_tree::ptree;
        ptree pt;
        boost::property_tree::read_xml(boost::filesystem::ifstream(p), pt);

        try {
            auto version = pt.get_child("root.version");
            _version = version.get_value<int>();

            BOOST_FOREACH(ptree::value_type const& v, pt.get_child("root.settings.int"))
            {
                std::string key = v.second.get<std::string>("name");
                _intOptions.insert(std::make_pair(key, v.second.get<int>("value")));
            }

            BOOST_FOREACH(ptree::value_type const& v, pt.get_child("root.settings.string"))
            {
                std::string key = v.second.get<std::string>("name");
                if (key == "ignores")
                {
                    Tokenizer builds(v.second.get<std::string>("value"), ',');
                    std::for_each(builds.begin(), builds.end(), [](const char* str) {
                        sMonitor->IgnoreBuild(str);
                    });
                }
                else
                    _strOptions.insert(std::make_pair(key, v.second.get<std::string>("value")));
            }
            return true;
        }
        catch (...)
        {
            return true;
        }
    }

    void write()
    {
        boost::filesystem::path p = boost::filesystem::initial_path();
        p /= "Config.xml";

        boost::property_tree::ptree pt;
        pt.add("root.version", _version + 1);
        BOOST_FOREACH (auto pair, _intOptions)
        {
            boost::property_tree::ptree& node = pt.add("root.settings.int", "");
            node.put("name", pair.first);
            node.put("value", pair.second);
        }

        BOOST_FOREACH (auto pair, _strOptions)
        {
            boost::property_tree::ptree& node = pt.add("root.settings.string", "");
            node.put("name", pair.first);
            node.put("value", pair.second);
        }

        write_xml(boost::filesystem::ofstream(p), pt,
            boost::property_tree::xml_writer_make_settings<std::string>(' ', 4));
    }

    bool _loaded = false;
};

#define sConfig ProgramConfig::instance()

#endif // ProgramConfig_h__
