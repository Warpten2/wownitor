#ifndef ConfigFile_h__
#define ConfigFile_h__

#include <unordered_map>
#include <string>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

class ConfigFile
{
public:
    ConfigFile(std::string const& fileUrl);

    typedef std::unordered_map<std::string, std::vector<std::string>> Container;

    std::vector<std::string>& operator [](std::string const& key)
    {
        return _data[key];
    }

// private:
    Container _data;
};

#endif // ConfigFile_h__