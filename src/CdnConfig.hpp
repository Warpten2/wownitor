#ifndef CdnConfig_h__
#define CdnConfig_h__

#include <unordered_map>
#include <string>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

class CdnConfig
{
public:
    CdnConfig(std::string const& fileUrl);

    typedef std::unordered_map<std::string, std::vector<std::string>> Container;

    std::vector<std::string>& operator [](std::string const& key)
    {
        return _data[key];
    }

    std::string GetHash() const
    {
        return _hash;
    }

    bool ContainsKey(const std::string& key) { return _data.find(key) != _data.end(); }

private:
    Container _data;
    std::string _hash;
};

#endif // CdnConfig_h__