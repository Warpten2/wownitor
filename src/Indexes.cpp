#include "Indexes.hpp"
#include "DataTypes.hpp"
#include "CdnConfig.hpp"
#include "Util.hpp"
#include "NetworkStream.h"

#include <iostream>
#include <boost/filesystem.hpp>

template<size_t N>
inline boost::filesystem::ifstream& operator >> (boost::filesystem::ifstream& strm, std::array<unsigned char, N>& arr)
{
    strm.read(reinterpret_cast<char*>(arr.data()), N);
    return strm;
}

template<size_t N>
inline boost::filesystem::ofstream& operator << (boost::filesystem::ofstream& strm, std::array<unsigned char, N>& arr)
{
    strm.write(reinterpret_cast<char*>(arr.data()), N);
    return strm;
}

Index::Index(std::string const& baseUrl, std::vector<std::string>& cdnConfig, std::string const& localArchiveGroupName) : _archiveNames(cdnConfig), _baseUrl(baseUrl)
{
    if (boost::filesystem::exists(localArchiveGroupName))
    {
        boost::filesystem::ifstream f(localArchiveGroupName, boost::filesystem::ifstream::binary);

        while (f.good())
        {
            std::array<unsigned char, 16> archiveHash;
            unsigned int entryCount;
            f >> archiveHash; // Works because of stream operator
            f.read(reinterpret_cast<char*>(&entryCount), 4);
            std::string archiveHashString = Util::to_hex_string(archiveHash);

            for (; entryCount > 0; --entryCount)
            {
                Entry entry;
                std::array<unsigned char, 16> entryHash;

                f >> entryHash; // Works because of operator

                f.read(reinterpret_cast<char*>(&(entry.Size)), 4); 
                f.read(reinterpret_cast<char*>(&(entry.Offset)), 4);
                entry.ArchiveHash = archiveHashString;

                _entries[entryHash] = entry;
            }
        }
    }
    else
    {
        boost::filesystem::ofstream f(localArchiveGroupName, std::ofstream::binary);

        static std::array<unsigned char, 16> zeroHash = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        int archiveIndex = 0;
        std::for_each(_archiveNames.begin(), _archiveNames.end(), [&](std::string const& archive)
        {
            std::string archiveUrl = baseUrl + "/data/" + archive.substr(0, 2) + "/" + archive.substr(2, 2) + "/" + archive + ".index";

            Network::Stream archiveFile(archiveUrl);

            int recordCount = archiveFile.content_length() / (16 + 4 + 4);
            f << Util::to_hex_array(archive);
            f.write(reinterpret_cast<char*>(&recordCount), 4);

            while (recordCount > 0)
            {
                Entry entry;
                std::array<unsigned char, 16> entryHash;

                archiveFile >> entryHash;
                if (entryHash == zeroHash)
                    archiveFile >> entryHash;

                archiveFile >> BigEndian<4>(entry.Size) >> BigEndian<4>(entry.Offset);
                entry.ArchiveHash = archive;

                f << entryHash; // Works because of the custom operator
                f.write(reinterpret_cast<char*>(&(entry.Size)), 4);
                f.write(reinterpret_cast<char*>(&(entry.Offset)), 4);

                _entries[entryHash] = entry;
                --recordCount;
            }

            ++archiveIndex;
        });

        f.close();
    }
}