#ifndef BLTEFile_h__
#define BLTEFile_h__

#include <vector>
#include <string>
#include <istream>
#include <iostream>
#include <streambuf>
#include <memory>

#include "DataTypes.hpp"
#include "NetworkStream.h"

#include <boost/variant.hpp>
#include <boost/filesystem/fstream.hpp>
#include <zlib.h>

class CDNHandler;

class BLTEFile
{
public:
    BLTEFile(std::string const& fileUrl, CDNHandler* cdnInfo);
    virtual ~BLTEFile();
    virtual void Load(Local::Buffer* variant) = 0;

    static bool Decompress(Network::Stream* binaryFile, Local::Buffer* decryptedFile);
private:
    std::unique_ptr<Local::Buffer> _localDecryptedFile;

protected:
    Local::Buffer* local_file() { return _localDecryptedFile.get(); }
};

#endif // BLTEFile_h__