#include "CDNHandler.hpp"
#include "CdnConfig.hpp"

#include "Indexes.hpp"
#include "Encoding.h"
#include "Install.hpp"
#include "Crypto.hpp"
#include "Root.hpp"

#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/regex.hpp>

std::vector<ExtractibleFile> CDNHandler::_extractingFiles = {
    ExtractibleFile("Wow.exe"),
    ExtractibleFile("Wow-64.exe"),

    ExtractibleFile("WowB.exe"),
    ExtractibleFile("WowB-64.exe"),

    ExtractibleFile("WowT.exe"),
    ExtractibleFile("WowT-64.exe"),

    ExtractibleFile("WowAE.exe"),
    ExtractibleFile("WowAE-64.exe"),

    ExtractibleFile("WowI.exe"),
    ExtractibleFile("WowI-64.exe"),

    ExtractibleFile("WowGM.exe"),
    ExtractibleFile("WowGM-64.exe"),

    ExtractibleFile("World of Warcraft Beta.app\\Contents\\MacOS\\World of Warcraft", "World of Warcraft Beta",
    [](boost::filesystem::path const& filePath, std::string const& extra) -> void
    {
        boost::system::error_code ec;
        boost::uintmax_t fileSize = boost::filesystem::file_size(filePath, ec);
        if (fileSize >= 45000000uLL)
            std::cout << std::endl << Util::GetTimeString() << "[" << extra << "] Possible magic binary !" << std::flush;
    }),
    ExtractibleFile("World of Warcraft.app\\Contents\\MacOS\\World of Warcraft", "World of Warcraft",
    [](boost::filesystem::path const& filePath, std::string const& extra) -> void
    {
        boost::system::error_code ec;
        boost::uintmax_t fileSize = boost::filesystem::file_size(filePath, ec);
        if (fileSize >= 45000000uLL)
            std::cout << std::endl << Util::GetTimeString() << "[" << extra << "] Possible magic binary !" << std::flush;
    }),
};

CDNHandler::CDNHandler(std::string const& baseUrl, std::string const& hash, std::string const& cdnConfig) : _baseUrl(baseUrl), _hash(hash)
{
    std::stringstream ss;
    ss << baseUrl << "/config/" << hash.substr(0, 2) << "/" << hash.substr(2, 2) << "/" << hash;

    _buildInfo = new CdnConfig(ss.str());

    const boost::regex buildNameRegex("WOW-([0-9]+)patch([0-9.]+)_([A-Za-z]+)", boost::regex::icase | boost::regex::extended);
    boost::cmatch regexMatch;
    if (boost::regex_match(GetName().c_str(), regexMatch, buildNameRegex))
    {
        std::stringstream name;
        name << regexMatch[3].str() << " " << regexMatch[2].str() << "." << regexMatch[1].str();
        _displayName = name.str().c_str();
    }

    ss.str(std::string());
    ss << baseUrl << "/config/" << cdnConfig.substr(0, 2) << "/" << cdnConfig.substr(2, 2) << "/" << cdnConfig;

    CdnConfig archives(ss.str());
    _archives = archives["archives"];
    _archiveGroup = archives["archive-group"][0];
    _rootHash = (*_buildInfo)["root"][0];

    if (!IsDumped())
        Dump();
}

CDNHandler::~CDNHandler()
{
    delete _buildInfo;
    delete _encoding;
    delete _install;
    delete _index;
    delete _root;
}

bool CDNHandler::IsDumped() const
{
    boost::filesystem::path p = GetPath();
    if (!boost::filesystem::exists(p))
        boost::filesystem::create_directories(p);
    p /= "dumped";
    return boost::filesystem::exists(p);
}

void CDNHandler::Dump()
{
    _thisThread = std::thread(&CDNHandler::ExtracterThread, this);
    _thisThread.detach();
}

void CDNHandler::ExtracterThread()
{
    boost::filesystem::path p = GetPath() / "dumped";
    boost::filesystem::ofstream dumped(p);
    dumped << 1;
    dumped.close();

    // 1. Download Encoding
    std::string encodingHash = (*_buildInfo)["encoding"][1];
    std::stringstream ss;
    ss << _baseUrl << "/data/" << encodingHash.substr(0, 2) << "/" << encodingHash.substr(2, 2) << "/" << encodingHash;
    std::string encodingUrl = ss.str();

    std::cout << std::endl << Util::GetTimeString() << "[" << GetDisplayName() << "] Loading encoding table ..." << std::flush;
    _encoding = new Encoding(encodingUrl, this);

    // 2. Download indexes
    std::cout << std::endl << Util::GetTimeString() << "[" << GetDisplayName() << "] Loading index table ..." << std::flush;
    _index = new Index(_baseUrl, _archives, (p.parent_path() / _archiveGroup).string());

    // 3. Download root
    Encoding::Entry rootEncoding = _encoding->GetEntry((*_buildInfo)["root"][0]);
    std::string rootHash = Util::to_hex_string(rootEncoding.Key);
    ss.str(std::string());
    ss << _baseUrl << "/data/" << rootHash.substr(0, 2) << "/" << rootHash.substr(2, 2) << "/" << rootHash;
    _root = new Root(ss.str(), this);

    // 4. If there is patch-config, download install
    if (_buildInfo->ContainsKey("patch-config"))
    {
        std::string patchConfig = (*_buildInfo)["patch-config"][0];
        ss.str(std::string());
        ss << _baseUrl << "/config/" << patchConfig.substr(0, 2) << "/" << patchConfig.substr(2, 2) << "/" << patchConfig;
        std::string patchConfigURL = ss.str();

        CdnConfig patchConfigFile(patchConfigURL);

        // Get install info - serves as a shortcut to root
        ss.str(std::string());
        ss << _baseUrl << "/data/" << patchConfigFile["install"][2].substr(0, 2) << "/" << patchConfigFile["install"][2].substr(2, 2) << "/" << patchConfigFile["install"][2];
        _install = new Install(ss.str(), this);
    }

    std::cout << std::endl << Util::GetTimeString() << "[" << GetDisplayName() << "] Extracting ..." << std::flush;

    std::for_each(_extractingFiles.begin(), _extractingFiles.end(), [&](ExtractibleFile const& file)
    {
        ExtractBinary(file.CDN, file.LocalPath);
        if (file.OnFileDownloadedCallback != nullptr)
            file.OnFileDownloadedCallback(GetPath() / file.LocalPath, GetDisplayName());
    });

    std::cout << std::endl << Util::GetTimeString() << "[" << GetDisplayName() << "] Found " << _dbFileNames.size() << " DBC references in binaries." << std::flush;

    Encoding::Entry encodingInfo;
    std::for_each(_dbFileNames.begin(), _dbFileNames.end(), [&](std::string dbcFile)
    {
        // Locate the file via hash
        std::cout << std::endl << Util::GetTimeString() << "[" << GetDisplayName() << "] Extracting '" << dbcFile << "' ..." << std::flush;

        std::uint64_t dbcFileHash = Crypto::ComputeHash(dbcFile);

        if (GetEncodingEntry(dbcFileHash, encodingInfo))
            DownloadClientDatabase(encodingInfo, GetPath() / dbcFile);

        // Try to get internal dbc while at it
        std::string::size_type extensionPos = dbcFile.find('.');
        dbcFile = dbcFile.insert(extensionPos, "_internal");
        dbcFileHash = Crypto::ComputeHash(dbcFile);

        if (GetEncodingEntry(dbcFileHash, encodingInfo))
            DownloadClientDatabase(encodingInfo, GetPath() / dbcFile);
    });

    std::cout << std::endl << Util::GetTimeString() << "[" << GetDisplayName() << "] Extracted " << _dbFileNames.size() << " data files." << std::flush;
    _dbFileNames.clear();
    std::cout << std::endl << Util::GetTimeString() << "[" << GetDisplayName() << "] Extracted !" << std::flush;
}

void CDNHandler::ExtractBinary(std::string const& filePath, std::string const& fileName)
{ 
    Encoding::Entry encodingInfo;
    if (!GetEncodingEntry(filePath, encodingInfo))
        return;

    std::cout << std::endl << Util::GetTimeString() << "[" << GetDisplayName() << "] Extracting '" << fileName.c_str() << "' ..." << std::flush;

    Network::Stream netStream;
    std::string url = _baseUrl + "/data/";

    // Look up index. If not null, use the targeted archive.
    if (_index->HasEntry(encodingInfo.Key))
    {
        Index::Entry const& indexInfo = _index->GetEntry(encodingInfo.Key);
        netStream.set_option(Network::Option::Range(indexInfo.Offset, indexInfo.Size));
        url += indexInfo.ArchiveHash.substr(0, 2) + "/" + indexInfo.ArchiveHash.substr(2, 2) + "/" + indexInfo.ArchiveHash.c_str();
    }
    else
    {
        std::string hash = Util::to_hex_string(encodingInfo.Key);
        url += hash.substr(0, 2) + "/" + hash.substr(2, 2) + "/" + hash.c_str();
    }

    netStream.open(url);

    // Decompress BLTE into a file.
    std::ofstream fs((GetPath() / fileName).native(), std::ofstream::binary);

    Local::Buffer decompressedFile;
    decompressedFile.write_sink(&fs);
    if (!BLTEFile::Decompress(&netStream, &decompressedFile))
        return;
    fs.close();

    // Look for DBCs in this binary
    std::ifstream file((GetPath() / fileName).native(), std::ios::binary | std::ios::ate);

    std::streampos length = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<char> buffer(length);
    file.read(buffer.data(), length);

    std::vector<char*> dbcFileNames = Util::FindPattern(buffer.data(), "DBFilesClient", length, 13);
    _dbFileNames.insert(dbcFileNames.begin(), dbcFileNames.end());
}

boost::filesystem::path CDNHandler::GetPath() const
{
    boost::filesystem::path p = boost::filesystem::initial_path();
    p /= "Dumps";
    p /= GetName() + "-" + GetHash();
    return p;
}

void CDNHandler::DownloadClientDatabase(Encoding::Entry const& encodingEntry, boost::filesystem::path const& filePath) const
{
    if (boost::filesystem::exists(filePath))
        return;

    if (!boost::filesystem::exists(filePath.parent_path()))
        boost::filesystem::create_directories(filePath.parent_path());

    Index::Entry indexEntry;
    indexEntry.Offset = 0;
    indexEntry.Size = 0;

    Network::Stream netStream;

    std::string fileHash = Util::to_hex_string(encodingEntry.Key);
    if (_index->HasEntry(encodingEntry.Key))
    {
        indexEntry = _index->GetEntry(encodingEntry.Key);
        netStream.set_option(Network::Option::Range(indexEntry.Offset, indexEntry.Size));
        fileHash = indexEntry.ArchiveHash;
    }

    std::stringstream URL;
    URL << _baseUrl;
    URL << "/data/" << fileHash.substr(0, 2) << "/" << fileHash.substr(2, 2) << "/" << fileHash;

    boost::filesystem::ofstream fileStream(filePath, std::ofstream::binary);

    Local::Buffer localBuffer;
    localBuffer.write_sink(&fileStream);

    bool success;
    int attempt_count = 0;
    do
    {
        netStream.open(URL.str());
        success = BLTEFile::Decompress(&netStream, &localBuffer);
        if (!success)
            ++attempt_count;
    } while (!success && attempt_count < 5);

    if (!success)
        std::cout << std::endl << Util::GetTimeString() << "[" << GetDisplayName() << "] Could not download '" << filePath.filename() << "." << std::flush;

    fileStream.close();
}

bool CDNHandler::GetEncodingEntry(uint64_t hash, Encoding::Entry& entry) const
{
    if (!_root->HasEntry(hash))
        return false;

    Root::Entry const& rootEntry = _root->GetEntry(hash);
    if (!_encoding->HasEntry(rootEntry.MD5))
        return false;

    entry = _encoding->GetEntry(rootEntry.MD5);
    return true;
}

bool CDNHandler::GetEncodingEntry(const std::string& fileName, Encoding::Entry& entry) const
{
    if (_install->HasEntry(fileName))
    {
        Install::Entry installEntry = _install->GetEntry(fileName)[0];
        bool hasEntry = _encoding->HasEntry(installEntry.MD5);
        if (hasEntry)
            entry = _encoding->GetEntry(installEntry.MD5);

        return hasEntry;
    }

    // Fallback to root
    return GetEncodingEntry(Crypto::ComputeHash(fileName), entry);
}
