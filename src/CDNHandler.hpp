#ifndef CDNHandler_h__
#define CDNHandler_h__

#include "CdnConfig.hpp"
#include "Encoding.h"

class ConfigFile;
class Index;
class Install;
class Root;

#include <sstream>
#include <string>
#include <thread>
#include <vector>
#include <set>
#include <functional>

#include <boost/filesystem/path.hpp>

struct ExtractibleFile
{
    explicit ExtractibleFile(const std::string& cdn)
    {
        CDN = cdn;
        LocalPath = cdn;
    }

    explicit ExtractibleFile(const std::string& cdn, std::function<void(boost::filesystem::path const&, std::string const&)> fn) :
        OnFileDownloadedCallback(fn)
    {
        CDN = cdn;
        LocalPath = cdn;
    }

    explicit ExtractibleFile(const std::string& cdn, const std::string& local, std::function<void(boost::filesystem::path const&, std::string const&)> fn) :
        OnFileDownloadedCallback(fn)
    {
        CDN = cdn;
        LocalPath = local;
    }

    explicit ExtractibleFile(const std::string& cdn, const std::string& local)
    {
        CDN = cdn;
        LocalPath = local;
    }

    std::string CDN;
    std::string LocalPath;
    std::function<void(boost::filesystem::path const&, std::string const&)> OnFileDownloadedCallback;
};

class CDNHandler
{
public:
    CDNHandler(std::string const& baseUrl, std::string const& hash, std::string const& cdnConfig);
    ~CDNHandler();

    void Dump();

    std::string const& GetDisplayName() const { return _displayName; }
    std::string const& GetName() const { return (*_buildInfo)["build-name"][0]; }
    std::string const& GetHash() const { return _hash; }

    bool IsDumped() const;

    void ExtractBinary(std::string const& fileName) { ExtractBinary(fileName, fileName); }
    void ExtractBinary(std::string const& filePath, std::string const& fileName);
    boost::filesystem::path GetPath() const;

private:
    void ExtracterThread();

    void DownloadClientDatabase(Encoding::Entry const& encodingEntry, boost::filesystem::path const& filePath) const;

    bool GetEncodingEntry(uint64_t hash, Encoding::Entry& entry) const;
    bool GetEncodingEntry(const std::string& fileName, Encoding::Entry& entry) const;

private:
    CdnConfig* _buildInfo;
    Encoding* _encoding;
    Index* _index;
    Install *_install;
    Root* _root;

    std::string _hash;
    std::string _baseUrl;
    std::string _rootHash;

    std::string _displayName;

    std::thread _thisThread;

    std::string _archiveGroup;
    std::vector<std::string> _archives;

    std::set<std::string> _dbFileNames;

    static std::vector<ExtractibleFile> _extractingFiles;
};

#endif // CDNHandler_h__
