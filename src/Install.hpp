#ifndef Install_h__
#define Install_h__

class CDNHandler;

#include "BLTEFile.hpp"
#include "DataTypes.hpp"
#include "NetworkStream.h"

#include <string>
#include <unordered_map>
#include <array>

class Install : public BLTEFile
{
public:
    explicit Install(std::string const& fileUrl, CDNHandler* buildInfo) : BLTEFile(fileUrl, buildInfo) { Load(local_file()); }

    void Load(Local::Buffer* decompressedFile) override;

    struct Entry
    {
        std::array<unsigned char, 16> MD5;
        int Size;
    };

    bool HasEntry(std::string const& fileName);
    std::vector<Entry>& GetEntry(std::string const& fileName);
private:
    typedef std::array<char, 16> Hash;

    std::unordered_map<std::string, std::vector<Entry>> _entries;
};

#endif // Encoding_h__