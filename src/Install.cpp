#include "Install.hpp"
#include "DataTypes.hpp"
#include "Util.hpp"
#include "NetworkStream.h"
#include <sstream>

void Install::Load(Local::Buffer* stream)
{
    short Signature;
    *stream >> Signature;

    stream->skip(2); // b, b

    short numTags;
    *stream >> BigEndian<2>(numTags);

    int numEntries = 0;
    *stream >> BigEndian<4>(numEntries);

    int numMaskBytes = numEntries / 8 + (numEntries % 8 > 0 ? 1 : 0);

    for (int i = 0; i < numTags; ++i)
    {
        std::string tag;
        *stream >> tag;
        stream->skip(numMaskBytes + 2);
    }

    for (int i = 0; i < numEntries; ++i)
    {
        Entry entry;
        std::string entryName;
        *stream >> entryName >> entry.MD5 >> BigEndian<4>(entry.Size);
        _entries[entryName].push_back(entry);
    }
}

bool Install::HasEntry(std::string const& fileName)
{
    return _entries.find(fileName) != _entries.end();
}

std::vector<Install::Entry>& Install::GetEntry(std::string const& fileName)
{
    return _entries.at(fileName);
}