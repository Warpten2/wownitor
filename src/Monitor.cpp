#include "Monitor.hpp"
#include "Util.hpp"
#include "Tokenizer.hpp"
#include "ConfigFile.hpp"
#include "CdnConfig.hpp"
#include "BLTEFile.hpp"
#include "Encoding.h"
#include "Install.hpp"
#include "Indexes.hpp"
#include "CDNHandler.hpp"
#include "ProgramConfig.hpp"

#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <sstream>
#include <ctime>
#include <iomanip>

#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>

Monitor::Monitor() { }

Monitor::~Monitor()
{
    if (!_requestStop)
        Stop();
}

bool Monitor::Stop()
{
    _requestStop = true;
    return true;
}

bool Monitor::Start()
{
    _requestStop = false;
    Poll();
    return true;
}

void Monitor::Poll()
{
    std::vector<CDNHandler*> _handlers;
    std::unordered_map<std::string, CdnConfig> referencedBuilds;
    while (!_requestStop)
    {
        try
        {
            std::string versionsUrl, cdnUrls;
            switch (_regionIndex)
            {
                case 0:
                    versionsUrl = cdnUrls = "http://us.patch.battle.net/";
                    break;
                case 1:
                default:
                    versionsUrl = cdnUrls = "http://eu.patch.battle.net/";
                    break;
            }

            versionsUrl += sConfig->GetString("channel") + "/versions";
            cdnUrls += sConfig->GetString("channel") + "/cdns";

            std::cout << std::endl << Util::GetTimeString() << "Loading versions " << std::flush;
            ConfigFile versions(versionsUrl);

            std::cout << "and CDNs ..." << std::flush;
            ConfigFile cdns(cdnUrls);

            std::stringstream ss;
            Tokenizer availableHosts(cdns["Hosts"][_regionIndex], ' ', 2);
            ss << "http://" << availableHosts[0] << "/" << cdns["Path"][0];
            std::string baseUrl = ss.str();
            ss << "/config/" << versions["CDNConfig"][_regionIndex].substr(0, 2) << "/"
                << versions["CDNConfig"][_regionIndex].substr(2, 2) << "/"
                << versions["CDNConfig"][_regionIndex];

            // List all available builds
            CdnConfig cdnConfig(ss.str());

            std::cout << std::endl << Util::GetTimeString() << "Discovered " << cdnConfig["builds"].size() << " client builds... " << std::flush;

            bool changesDetected = false;

            for (std::vector<CDNHandler*>::iterator i = _handlers.begin(); i != _handlers.end();)
            {
                std::vector<std::string>::const_iterator itr = std::find_if(cdnConfig["builds"].begin(), cdnConfig["builds"].end(), [&i](std::string const& hash) {
                    return hash == (*i)->GetHash();
                });
                if (itr == cdnConfig["builds"].end())
                {
                    std::cout << std::endl << Util::GetTimeString() << "[*] " << (*i)->GetName() << ": Build deleted!" << std::flush;
                    delete (*i);
                    i = _handlers.erase(i);
                    changesDetected = true;
                }
                else
                    ++i;
            }

            for (unsigned int i = 0; i < cdnConfig["builds"].size(); ++i)
            {
                std::string buildHash = cdnConfig["builds"][i];
                std::vector<CDNHandler*>::const_iterator itr = std::find_if(_handlers.begin(), _handlers.end(), [&buildHash](CDNHandler* handler) {
                    return handler->GetHash() == buildHash;
                });

                if (itr == _handlers.end())
                {
                    changesDetected = true;

                    if (IsIgnored(cdnConfig["builds"][i]))
                        continue;

                    CDNHandler* handler = new CDNHandler(baseUrl, cdnConfig["builds"][i], versions["CDNConfig"][_regionIndex]);
                    std::cout << std::endl << Util::GetTimeString() << "[*] " << handler->GetDisplayName() << ": Build deployed!" << std::flush;
                    _handlers.push_back(handler);
                }
            }

            if (!changesDetected)
                std::cout << "No change detected.";
        }
        catch (...) { /* Do nothing, this might be due to network errors. */ }
        std::this_thread::sleep_for(std::chrono::minutes(1)); // 1 minute
    }
}
