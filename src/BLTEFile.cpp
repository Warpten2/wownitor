#include "BLTEFile.hpp"
#include "DataTypes.hpp"
#include "NetworkStream.h"
#include "Util.hpp"
#include "CDNHandler.hpp"

#include <istream>
#include <iostream>
#include <streambuf>
#include <fstream>
#include <zlib.h>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/asio/detail/buffered_stream_storage.hpp>

BLTEFile::~BLTEFile()
{
}

BLTEFile::BLTEFile(std::string const& fileUrl, CDNHandler* handler)
{
    boost::filesystem::path filePath = handler->GetPath();
    std::string hash = fileUrl.substr(fileUrl.rfind('/') + 1);
    filePath /= hash;

    if (boost::filesystem::exists(filePath))
    {
        boost::filesystem::ifstream ifs(filePath, boost::filesystem::ifstream::binary | boost::filesystem::ifstream::ate);
        boost::filesystem::ifstream::pos_type pos = ifs.tellg();

        std::vector<unsigned char> d;
        d.resize(pos);

        ifs.seekg(0, boost::filesystem::ifstream::beg);
        ifs.read((char*)d.data(), pos);
        ifs.close();

        _localDecryptedFile = std::make_unique<Local::Buffer>(d);
    }
    else
    {
        Network::Stream fileStream(fileUrl);

        std::ofstream outputFile(filePath.c_str(), std::ios::binary);
        _localDecryptedFile = std::make_unique<Local::Buffer>();
        _localDecryptedFile->write_sink(&outputFile);
        Decompress(&fileStream, _localDecryptedFile.get());
    }
}

bool BLTEFile::Decompress(Network::Stream* binaryFile, Local::Buffer* decryptedFile)
{
    int fileSignature;
    int headerSize;

    *binaryFile >> fileSignature >> BigEndian<4>(headerSize);

    short chunkCount;
    std::vector<int> compressedChunkSizes;

    if (headerSize > 0)
    {
        short flags;
        *binaryFile >> BigEndian<2>(flags) >> BigEndian<2>(chunkCount);
        compressedChunkSizes.resize(chunkCount);
        for (int i = 0; i < chunkCount; ++i)
        {
            int compressedSize, decompressedSize;
            *binaryFile >> BigEndian<4>(compressedSize) >> BigEndian<4>(decompressedSize);
            binaryFile->skip(16);
            compressedChunkSizes[i] = compressedSize - 1;
        }
    }
    else
    {
        chunkCount = 1;
        compressedChunkSizes.push_back(binaryFile->content_length() - 9);
    }

    for (int i = 0; i < chunkCount; ++i)
    {
        char encodingMode;
        std::vector<unsigned char> chunkData(compressedChunkSizes[i]);
        *binaryFile >> encodingMode >> chunkData;
        switch (encodingMode)
        {
            case 0x4E: // Raw data
                *decryptedFile << chunkData;
                break;
            case 0x5A: // ZLib
            {
                z_stream strm;
                strm.zalloc = Z_NULL;
                strm.zfree = Z_NULL;
                strm.opaque = Z_NULL;
                strm.avail_in = 0;
                strm.next_in = Z_NULL;
                int ret = inflateInit(&strm);
                if (ret != Z_OK)
                    break;

                strm.avail_in = chunkData.size();
                strm.next_in = &chunkData[0];

                const int BufferSize = 4096;
                unsigned char outputBuffer[BufferSize];
                while (ret != Z_STREAM_END && strm.avail_in != 0)
                {
                    strm.avail_out = BufferSize;
                    strm.next_out = &outputBuffer[0];

                    ret = inflate(&strm, Z_NO_FLUSH);
                    if (ret < 0)
                    {
                        std::cout << "Zlib error " << std::hex << ret << " when decompressing BLTE archive. Process cancelled." << std::endl;
                        ret = inflateEnd(&strm);
                        return false;
                    }

                    // ASSERT(ret == Z_STREAM_ERROR, "Clobbered zlib state");
                    for (unsigned int i = 0; i < BufferSize - strm.avail_out; ++i)
                        *decryptedFile << outputBuffer[i];
                }

                ret = inflateEnd(&strm);
                break;
            }
            default:
                ASSERT(true, "Invalid compression algorithm");
                break;
        }
    }
    return true;
}
