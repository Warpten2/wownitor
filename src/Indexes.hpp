#ifndef Indexes_h__
#define Indexes_h__

#include "CdnConfig.hpp"
#include "Util.hpp"

#include <unordered_map>

class Index
{
public:
    Index(std::string const& baseUrl, std::vector<std::string>& archives, std::string const& localArchiveGroupName);

    struct Entry
    {
        std::string ArchiveHash;
        int Offset;
        int Size;
    };

    Entry const& GetEntry(std::array<unsigned char, 16> const& key) const
    {
        return _entries.at(key);
    }

    bool HasEntry(std::array<unsigned char, 16> const& key) const
    {
        return _entries.find(key) != _entries.end();
    }

    Entry const& GetEntry(std::string const& key) const { return GetEntry(Util::to_hex_array(key)); }
    bool HasEntry(std::string const& key) const { return HasEntry(Util::to_hex_array(key)); }

    std::unordered_map<std::array<unsigned char, 16>, Entry> _entries;
    std::vector<std::string> _archiveNames;
    std::string _baseUrl;
};

#endif