#include "CdnConfig.hpp"
#include "Tokenizer.hpp"
#include "NetworkStream.h"
#include "Util.hpp"

CdnConfig::CdnConfig(std::string const& fileUrl)
{
    _hash = fileUrl.substr(fileUrl.rfind('/') + 1);
    Network::Stream fileStream(fileUrl);
    Tokenizer lines(fileStream.string(), '\n');

    std::string previousKey;
    std::for_each(lines.begin(), lines.end(), [&](const std::string& line)
    {
        if (line.length() == 0)
            return;
        
        std::string currentKey = Util::trim(line.substr(0, line.find('=')));
        std::string currentValue = Util::trim(line.substr(line.find('=') + 1));

        if (previousKey == currentKey)
        {
            currentKey = currentValue.substr(0, currentValue.find(' '));
            currentValue = currentValue.substr(currentValue.find(' ') + 1);
        }

        if (previousKey != currentKey)
            previousKey = currentKey;

        Tokenizer values(currentValue, ' ', 10);

        for (const char* item : values)
            if (strlen(item) > 0)
                _data[currentKey].push_back(Util::trim(std::string(item)));
    });
}