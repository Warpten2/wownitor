#ifndef Monitor_h__
#define Monitor_h__

class CDNHandler;
class CdnConfig;
class Encoding;
class Install;

#include "Indexes.hpp"

#include <string>
#include <vector>
#include <unordered_map>

class Monitor
{
public:
    static Monitor* instance()
    {
        static Monitor monitor;
        return &monitor;
    }

    bool Start();
    bool Stop();

    void Poll();
    void SetRegionIndex(int regionIndex) { _regionIndex = regionIndex; }


    bool IsIgnored(const std::string& build)
    {
        return std::find(_ignoredBuilds.begin(), _ignoredBuilds.end(), build) != _ignoredBuilds.end();
    }

    void IgnoreBuild(const std::string& build)
    {
        if (IsIgnored(build))
            return;

        _ignoredBuilds.push_back(build);
    }

private:
    Monitor();
    ~Monitor();

    int _regionIndex;
    bool _requestStop;

    std::vector<std::string> _ignoredBuilds;
};

#define sMonitor Monitor::instance()

#endif // Monitor_h__