#include "ConfigFile.hpp"

#include "Util.hpp"
#include "Tokenizer.hpp"
#include "NetworkStream.h"

ConfigFile::ConfigFile(std::string const& fileUrl)
{
    /*if (!Util::DownloadText(fileUrl, lines))
        return;*/

    Network::Stream fileStream(fileUrl);
    Tokenizer lines(fileStream.string(), '\n');

    std::vector<std::string> sections;
    for (const char* fileSection : Tokenizer(lines[0], '|', 5))
    {
        Tokenizer tokens(fileSection, '!', 2);
        sections.push_back(Util::trim(std::string(tokens[0])));
    }

    std::for_each(lines.begin() + 1, lines.end(), [&](const std::string& line) 
    {
        Tokenizer lineSplits = Tokenizer(line, '|', 5);

        for (unsigned int columnIndex = 0; columnIndex < lineSplits.size(); ++columnIndex)
        {
            if (_data.find(sections[columnIndex]) == _data.end())
                _data[sections[columnIndex]] = { };

            _data[sections[columnIndex]].push_back(Util::trim(std::string(lineSplits[columnIndex])));
        }
    });
}