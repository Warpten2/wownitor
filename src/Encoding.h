#ifndef Encoding_h__
#define Encoding_h__

#include "BLTEFile.hpp"
#include "DataTypes.hpp"
#include "Util.hpp"

class CDNHandler;

#include <string>
#include <unordered_map>
#include <array>
#include <boost/interprocess/streams/bufferstream.hpp>

class Encoding : public BLTEFile
{
public:
    explicit Encoding(std::string const& fileUrl, CDNHandler* buildInfo) : BLTEFile(fileUrl, buildInfo)
    {
        Load(local_file());
    }

    void Load(Local::Buffer* decompressedFile) override;

    struct Entry
    {
        int FileSize;
        std::array<unsigned char, 16> Key;
    };

    bool HasEntry(std::string const& hashString) const { return HasEntry(Util::to_hex_array(hashString)); }
    Entry const& GetEntry(std::string const& hashString) const { return GetEntry(Util::to_hex_array(hashString)); }

    bool HasEntry(std::array<unsigned char, 16> const& hashString) const;
    Entry const& GetEntry(std::array<unsigned char, 16> const& hashString) const;

private:
    typedef std::array<unsigned char, 16> Hash;

    std::unordered_map<Hash, Entry> _entries;
};

#endif // Encoding_h__