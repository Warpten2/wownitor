#ifndef NetworkStream_h__
#define NetworkStream_h__

#include <string>
#include <sstream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <algorithm>
#include <vector>
#include <memory>

#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>

#include "DataTypes.hpp"

namespace Network
{
    class NetworkOption
    {
        public:
            NetworkOption() { }

            std::string name() const { return _name; }
            std::string body() const { return _body; }

        protected:
            std::string _name;
            std::string _body;
    };

    namespace Option
    {
        class Accept : public NetworkOption
        {
        public:
            Accept(const std::string& range) : NetworkOption()
            {
                _name = "Accept";
                _body = range;
            }

            Accept() : NetworkOption()
            {
                _name = "Accept";
                _body = "*/*";
            }
        };

        class Range : public NetworkOption
        {
        public:
            Range(uint64_t offset, uint64_t range) : NetworkOption()
            {
                _name = "Range";
                std::stringstream ss;
                ss << "bytes=" << offset << "-" << (offset + range - 1);
                _body = ss.str();
            }
        };

        class ContentLength : public NetworkOption
        {
        public:
            ContentLength(const std::string& range) : NetworkOption()
            {
                _name = "Content-Length";
                _body = range;
            }
        };

        class ContentType : public NetworkOption
        {
        public:
            ContentType(const std::string& range) : NetworkOption()
            {
                _name = "Content-Type";
                _body = range;
            }
        };

        class UserAgent : public NetworkOption
        {
        public:
            UserAgent(const std::string& range) : NetworkOption()
            {
                _name = "User-Agent";
                _body = range;
            }
        };
    }

    class Stream
    {
    public:
        explicit Stream(const std::string& fileUrl);
        explicit Stream();

        Stream& operator = (Stream const& lhs) = delete;
        Stream(Stream const& lhs) = delete;
        Stream(Stream&& lhs) = delete;

        ~Stream()
        {
            _stream.close();
        }

        bool open(const std::string& fileUrl);
        bool is_open() const;

        template <typename T>
        T* get_option(const std::string& optionName) const
        {
            for (std::vector<NetworkOption>::const_iterator itr = _options.begin(); itr != _options.end(); ++itr)
                if (itr->name() == optionName)
                    return reinterpret_cast<T*>(*itr);
            return nullptr;
        }

        void read_many(std::uint64_t size)
        {
            std::vector<char> buffer(std::min(content_length() - position(), size));
            _stream.read(buffer.data(), buffer.size());
            if (_read_sink != nullptr)
                _read_sink->write(buffer.data(), buffer.size());
        }

        void read_to_end()
        {
            std::vector<char> buffer(content_length() - position());
            _stream.read(buffer.data(), buffer.size());
            if (_read_sink != nullptr)
                _read_sink->write(buffer.data(), buffer.size());
        }

        void set_option(Network::NetworkOption const& option);

        template <typename T> Stream& operator >> (std::vector<T>& v)
        {
            _position += v.size();
            _stream.read(reinterpret_cast<char*>(v.data()), sizeof(T) * v.size());
            if (_read_sink != nullptr)
                _read_sink->write(reinterpret_cast<char*>(v.data()), sizeof(T) * v.size());
            return *this;
        }

        Stream& operator >> (std::string& s)
        {
            std::getline(_stream, s, '\0');
            if (_read_sink != nullptr)
                _read_sink->write(s.c_str(), s.length());
            return *this;
        }

        void read_sink(std::ostream* os)
        {
            _read_sink = os;
        }

        template <typename T> Stream& operator >> (T& v)
        {
            size_t queriedSize = sizeof(T);
            _stream.read(reinterpret_cast<char*>(&v), queriedSize);
            if (_read_sink != nullptr)
                _read_sink->write(reinterpret_cast<char*>(&v), queriedSize);
            _position += queriedSize;
            return *this;
        }

        template <size_t N> Stream& operator >> (BigEndian<N>& be)
        {
            _position += N;
            for (size_t i = 0; i < N; ++i)
            {
                _stream.read(reinterpret_cast<char*>(&be[i]), 1);
                if (_read_sink != nullptr)
                    _read_sink->write(reinterpret_cast<char*>(&be[i]), 1);
            }
            return *this;
        }

        template <typename T, size_t N> Stream& operator >> (std::array<T, N>& arr)
        {
            size_t queriedSize = N * sizeof(T);
            _stream.read(reinterpret_cast<char*>(arr.data()), N * sizeof(T));
            if (_read_sink != nullptr)
                _read_sink->write(reinterpret_cast<char*>(arr.data()), N * sizeof(T));
            _position += queriedSize;
            return *this;
        }

        void skip(size_t n)
        {
            std::vector<char> v(n);
            _stream.read(reinterpret_cast<char*>(v.data()), v.size());
            if (_read_sink != nullptr)
                _read_sink->write(reinterpret_cast<char*>(v.data()), v.size());
            _position += n;
        }

        std::uint64_t content_length() const { return _content_length; }
        std::uint64_t position() const { return _position; }

        std::string string()
        {
            std::stringstream ss;
            bool c = true;
            do {
                std::string line;
                std::getline(_stream, line);
                ss << line << '\n';
            } while (!_stream.eof());
            return ss.str();
        }

    private:
        std::uint64_t _content_length = 0;
        std::uint64_t _position = 0;

        std::ostream* _read_sink = nullptr;

        boost::asio::ip::tcp::iostream _stream;

        std::string _protocol;
        std::string _host;
        std::string _path;
        std::string _query;

        std::vector<NetworkOption> _options;
    };
}

namespace Local
{
    class Buffer
    {
    public:
        Buffer() : _writeStream(&_buffer), _readStream(&_buffer) { }

        Buffer(std::vector<unsigned char>& data) : _writeStream(&_buffer), _readStream(&_buffer)
        {
            _size = data.size();

            _writeStream.write(reinterpret_cast<char*>(data.data()), data.size());
        }

        ~Buffer()
        {
        }

        template <typename T>
        Buffer& operator >> (std::vector<T>& v)
        {
            _position += sizeof(T) * v.size();
            _readStream.read(reinterpret_cast<char*>(v.data()), v.size() * sizeof(T));
            if (_read_sink != nullptr)
                _read_sink->write(reinterpret_cast<char*>(v.data()), sizeof(T) * v.size());
            return *this;
        }

        template<size_t N>
        Buffer& operator >> (BigEndian<N>& be)
        {
            for (size_t i = 0; i < N; ++i)
                be[i] = read<unsigned char>();
            return *this;
        }

        template <typename T>
        Buffer& operator >> (T& v)
        {
            v = read<T>();
            return *this;
        }

        Buffer& operator >> (std::string& s)
        {
            std::getline(_readStream, s, '\0');
            if (_read_sink != nullptr)
                _read_sink->write(s.c_str(), s.length());
            return *this;
        }

        template <typename T, size_t N>
        Buffer& operator >> (std::array<T, N>& arr)
        {
            for (size_t i = 0; i < N; ++i)
                arr[i] = read<T>();
            return *this;
        }

        // Writes
        template <typename T>
        Buffer& operator << (std::vector<T>& v)
        {
            _writeStream.write(reinterpret_cast<char*>(v.data()), v.size() * sizeof(T));
            if (_write_sink != nullptr)
                _write_sink->write(reinterpret_cast<char*>(v.data()), v.size() * sizeof(T));
            return *this;
        }

        template<size_t N>
        Buffer& operator << (BigEndian<N>& be)
        {
            for (size_t i = 0; i < N; ++i)
            {
                _writeStream.write(reinterpret_cast<char*>(&be[i]), 1);
                if (_write_sink != nullptr)
                    _write_sink->write(reinterpret_cast<char*>(&be[i]), 1);
            }
            return *this;
        }

        template <typename T>
        Buffer& operator << (T& v)
        {
            _writeStream.write(reinterpret_cast<char*>(&v), sizeof(T));
            if (_write_sink != nullptr)
                _write_sink->write(reinterpret_cast<char*>(&v), sizeof(T));
            return *this;
        }

        template <typename T, size_t N>
        Buffer& operator << (std::array<T, N>& arr)
        {
            _writeStream.write(reinterpret_cast<char*>(arr.data()), N * sizeof(T));
            if (_write_sink != nullptr)
                _write_sink->write(reinterpret_cast<char*>(arr.data()), N * sizeof(T));
            return *this;
        }

        void skip(size_t n)
        {
            std::vector<char> t;
            t.resize(n);
            _position += n;
            _readStream.read(t.data(), t.size());
            if (_read_sink != nullptr) // write garbage as filler
                _read_sink->write(t.data(), t.size());
        }

        void read_sink(std::ostream* s) { _read_sink = s; }
        void write_sink(std::ostream* s) { _write_sink = s; }

        bool read_good() { return _readStream.good(); }

        uint64_t position() { return _position; }
        uint64_t size() { return _size; }
    private:

        template <typename T>
        T read()
        {
            T v;
            _position += sizeof(T);
            _readStream.read(reinterpret_cast<char*>(&v), sizeof(T));
            if (_read_sink != nullptr)
                _read_sink->write(reinterpret_cast<char*>(&v), sizeof(T));
            return v;
        }

        boost::asio::streambuf _buffer;
        std::ostream _writeStream;
        std::ostream* _write_sink = nullptr;
        std::istream _readStream;
        std::ostream* _read_sink = nullptr;

        std::uint64_t _position = 0;
        std::uint64_t _size = 0;
    };
}

#endif // NetworkStream_h__
