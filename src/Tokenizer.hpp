#ifndef Tokenizer_h__
#define Tokenizer_h__

#include <string>
#include <vector>
#include <cstdint>

class Tokenizer
{
public:
    typedef std::vector<char const*> StorageType;

    typedef StorageType::size_type size_type;

    typedef StorageType::const_iterator const_iterator;
    typedef StorageType::reference reference;
    typedef StorageType::const_reference const_reference;

    typedef StorageType::const_reverse_iterator const_reverse_iterator;

public:
    Tokenizer(const std::string &src, char const sep, int vectorReserve = 0) {
        Tokenize(src, sep, vectorReserve);
    }

    void Tokenize(const std::string &src, char const sep, int vectorReserve = 0);

    ~Tokenizer() { delete[] m_str; }

    const_iterator begin() const { return m_storage.begin(); }
    const_iterator end() const { return m_storage.end(); }

    const_reverse_iterator rbegin() const { return m_storage.rbegin(); }
    const_reverse_iterator rend() const { return m_storage.rend(); }

    size_type size() const { return m_storage.size(); }

    reference operator [] (size_type i) { return m_storage[i]; }
    const_reference operator [] (size_type i) const { return m_storage[i]; }

    std::vector<std::string> ToVector();

private:
    char* m_str = NULL;
    StorageType m_storage;
};

#endif