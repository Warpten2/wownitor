#ifndef Util_h__
#define Util_h__

#include <string>
#include <vector>
#include <chrono>
#include <thread>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <array>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

#include <stdio.h>
#include <string.h>
#ifndef WIN32
# include <sys/time.h>
#endif
#include <stdlib.h>
#include <errno.h>

#include "CdnConfig.hpp"

#include <boost/filesystem.hpp>

#ifndef NDEBUG
#   define ASSERT(condition, msg) \
    do { \
        if (!(condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ << " line " << __LINE__ << ": " << msg << std::endl; \
            std::exit(EXIT_FAILURE); \
                        } \
            } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

#include <boost/filesystem/fstream.hpp>

namespace std
{
    template<typename T, size_t N>
    struct hash < array<T, N> >
    {
        typedef array<T, N> argument_type;
        typedef size_t result_type;

        result_type operator()(const argument_type& a) const
        {
            hash<T> hasher;
            result_type h = 0;
            for (result_type i = 0; i < N; ++i)
                h = h * 31 + hasher(a[i]);
            return h;
        }
    };
}

namespace Util
{
    template <template<typename...> class R = std::vector,
        typename Top,
        typename Sub = typename Top::value_type>
        R<typename Sub::value_type> flatten(Top const& all)
    {
        using std::begin;
        using std::end;

        R<typename Sub::value_type> accum;

        for (auto& sub : all)
            accum.insert(end(accum), begin(sub), end(sub));

        return accum;
    }

    static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
    }

    // trim from end
    static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
    }

    // trim from both ends
    static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
    }

    static inline std::string to_hex_string(std::array<unsigned char, 16> const& hashString)
    {
        std::ostringstream ss;
        ss << std::hex << std::setfill('0');
        for (int i = 0; i < 16; ++i)
            ss << std::setw(2) << static_cast<int>(hashString[i]);
        return ss.str();
    }

    static inline std::array<unsigned char, 16> to_hex_array(std::string const& hashString)
    {
        std::stringstream ss;
        std::array<unsigned char, 16> arr;
        unsigned int offset = 0;
        while (offset < hashString.length())
        {
            int buffer = 0;
            ss.clear();
            ss << std::hex << hashString.substr(offset, 2);
            ss >> buffer;
            arr[offset / 2] = static_cast<unsigned char>(buffer);
            offset += 2;
        }
        return arr;
    }

    static bool FileExists(std::string const& filePath)
    {
        return boost::filesystem::exists(filePath);
    }

    static std::string GetTimeString()
    {
        std::time_t t = std::time(NULL);
        std::stringstream ss;
        ss << "[" << std::put_time(std::localtime(&t), "%H:%M:%S") << "] ";
        return ss.str();
    }

    static std::vector<char*> FindPattern(char* inputArray, char* pattern, long inputLength, int patternLength)
    {
        std::vector<char*> result;
        char* begin = inputArray;
        char* end = inputArray + inputLength;

        long i = 0;
        for (; begin < end; ++i, ++begin)
        {
            bool matchFound = true;
            for (char *hInc = begin, *nInc = pattern, *nEnd = pattern + patternLength;
                matchFound && nInc < nEnd;
                matchFound = *nInc == *hInc, ++nInc, ++hInc);
            if (matchFound)
                result.push_back(inputArray + i);
        }

        return result;
    }
}

#endif