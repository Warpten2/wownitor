#include "Util.hpp"
#include "ProgramConfig.hpp"
#include "Monitor.hpp"

#include <iostream>
#include <chrono>
#include <thread>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include <windows.h>
#endif

void ClearConsole()
{
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    COORD topLeft = { 0, 0 };
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO screen;
    DWORD written;

    GetConsoleScreenBufferInfo(console, &screen);
    FillConsoleOutputCharacterA(
        console, ' ', screen.dwSize.X * screen.dwSize.Y, topLeft, &written
        );
    FillConsoleOutputAttribute(
        console, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE,
        screen.dwSize.X * screen.dwSize.Y, topLeft, &written
        );
    SetConsoleCursorPosition(console, topLeft);
#else
    // CSI[2J clears screen, CSI[H moves the cursor to top-left corner
    std::cout << "\x1B[2J\x1B[H";
#endif
}

int main()
{
    if (!sConfig->Loaded())
    {
        int regionIndex = 5;
        while (regionIndex < 0 || regionIndex > 4)
        {
            ClearConsole();
            std::cout << "Please indicate your region: " << std::endl;
            std::cout << "[0]: us" << std::endl;
            std::cout << "[1]: eu" << std::endl;
            std::cout << "[2]: kr" << std::endl;
            std::cout << "[3]: tw" << std::endl;
            std::cout << "[4]: cn" << std::endl << "> ";
            std::cin >> regionIndex;
        }

        ClearConsole();
        sMonitor->SetRegionIndex(regionIndex);
    }
    else
        sMonitor->SetRegionIndex(sConfig->GetInt("region"));

    if (!sMonitor->Start())
        return 1;

    return 0;
}