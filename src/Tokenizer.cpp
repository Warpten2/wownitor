#include "Tokenizer.hpp"

#include <sstream>
#include <string>
#include <cctype>
#include <unordered_map>
#include <vector>

void Tokenizer::Tokenize(const std::string &src, const char sep, int vectorReserve)
{
    if (m_str != nullptr)
        delete[] m_str;
    m_storage.clear();

    m_str = new char[src.length() + 1];
    memcpy(m_str, src.c_str(), src.length() + 1);

    if (vectorReserve)
        m_storage.reserve(vectorReserve);

    char* posold = m_str;
    char* posnew = m_str;

    for (;;)
    {
        if (*posnew == sep)
        {
            m_storage.push_back(posold);
            posold = posnew + 1;

            *posnew = '\0';
        }
        else if (*posnew == '\0')
        {
            // Hack like, but the old code accepted these kind of broken strings,
            // so changing it would break other things
            if (posold != posnew)
                m_storage.push_back(posold);

            break;
        }

        ++posnew;
    }
}

std::vector<std::string> Tokenizer::ToVector()
{
    std::vector<std::string> lines;
    for (const char* i : m_storage)
        lines.push_back(std::string(i));
    return lines;
}