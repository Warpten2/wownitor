#include "Root.hpp"
#include "DataTypes.hpp"
#include "Util.hpp"
#include <sstream>

std::array<char, 16> to_hex_array(std::string const& hashString);

void Root::Load(Local::Buffer* stream)
{
    while (stream->position() < stream->size())
    {
        int count;
        *stream >> count;

        Block block;
        int contentFlags, localeFlags;
        *stream >> contentFlags >> localeFlags;
        block.Content = (Root::ContentFlags)contentFlags;
        block.Locale = (Root::LocaleFlags)localeFlags;

        if (block.Locale == LocaleFlags::NoLocale)
            break; // Well, fuck

        std::vector<Entry> entries(count);
        int fileDataIndex = 0;
        for (int i = 0; i < count; ++i)
        {
            entries[i].RootBlock = block;
            int fileDataId;
            *stream >> fileDataId;
            entries[i].FileDataID += fileDataIndex + fileDataId;

            fileDataIndex = entries[i].FileDataID + 1;
        }

        for (int i = 0; i < count; ++i)
        {
            uint64_t hash;
            *stream >> entries[i].MD5 >> hash;
            _entries.insert(std::make_pair(hash, entries[i]));
        }
    }
}

bool Root::HasEntry(uint64_t hashString)
{
    return _entries.find(hashString) != _entries.end();
}

Root::Entry const& Root::GetEntry(uint64_t hashString)
{
    return _entries[hashString];
}
