# output generic information
if( UNIX )
  message("")
  message("* Wownitor buildtype      : ${CMAKE_BUILD_TYPE}")
endif()

message("")

# output information about installation-directories and locations

message("* Install Wownitor to       : ${CMAKE_INSTALL_PREFIX}")
message("")

# Show infomation about the options selected during configuration

message("")

